
import 'package:anime_force/src/core/component/anime/domain/website.dart';

abstract class Parser {
  AnimeListPage parseAnimeListPage(String webPage);
  AnimePage parseAnimePage(String html, Uri websiteUrl);
  StreamingPage parseStreamingPage(String html);
}

