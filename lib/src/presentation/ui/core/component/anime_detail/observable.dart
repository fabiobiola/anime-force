import 'package:anime_force/src/core/component/anime/domain/website.dart';
import 'package:anime_force/src/core/port/access.dart';
import 'package:anime_force/src/core/port/parsing.dart';
import 'package:mobx/mobx.dart';

part 'observable.g.dart';

class AnimeDetailStore = _AnimeDetailStore with _$AnimeDetailStore;

abstract class _AnimeDetailStore with Store {
  final AnimeForceWebsite _animeForceWebsite;
  final Parser _parser;

  _AnimeDetailStore(this._animeForceWebsite, this._parser);

  @observable
  AnimePage anime;

  @observable
  Map<EpisodeLink, StreamingPage> episodes = Map();

  @action
  Future<void> loadAnime(Uri url) async {
    String html = await _animeForceWebsite.fetchAnime(url);
    anime = _parser.parseAnimePage(html, url);

    for (var link in anime.episodes.where((episode) => episode.downloadable)) {
      String streamingPage =
          await _animeForceWebsite.fetchStreamingPage(link.uri);
      var episode = _parser.parseStreamingPage(streamingPage);
      if (episode != null) {
        episodes[link] = episode;
      }
    }

    // notify episodes to be rebuilt
    episodes = episodes;
  }
}
