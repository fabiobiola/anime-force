import 'package:equatable/equatable.dart' as ext;

abstract class Equatable extends ext.Equatable {

  @override
  String toString() => props.map((prop) => prop.toString()).join(",\n");
}
