import 'package:anime_force/src/core/component/anime/domain/website.dart';

// lists

final animeLinks = [
  AnimeLink(
    name: 'Black Clover',
    url: Uri.parse('/black-clover/'),
  ),
  AnimeLink(
    name: 'Another cool anime',
    url: Uri.parse('/another-cool-anime/'),
  ),
  AnimeLink(
    name: 'The coolest anime',
    url: Uri.parse('/the-coolest-anime/'),
  ),
];

final animeDetailMap = {
  '/black-clover/': blackClover,
  '/another-cool-anime/': anotherCoolAnime,
  '/the-coolest-anime/': theCoolestAnime,
};

final streamingByEpisodeLink = {
  episode1: StreamingPage(Uri.parse(
      'www.noughties.net/DDL/ANIME/25-saiNoJoshikousei/25-saiNoJoshikousei_Ep_01_SUB_ITA.mp4')),
};

// Details

final blackClover = AnimePage(
  name: 'Black Clover',
  genre: ['One', 'Second', 'Third'],
  plot: 'A simple plot of a cool anime',
  yearOfPublication: '2019',
  imageUrl: 'https://lorempixel.com/350/500/',
  episodes: [episode1],
);

final anotherCoolAnime = AnimePage(
  name: 'Another cool anime',
  genre: ['To be watch', 'Great', 'Cool'],
  plot: "It's old, but it's great",
  yearOfPublication: '2010',
  imageUrl: 'http://lorempixel.com/350/500/',
  episodes: [],
);

final theCoolestAnime = AnimePage(
  name: 'The coolest anime',
  genre: ['Genre1', 'Genre2', 'Genre3'],
  plot: 'This one is really cool',
  yearOfPublication: '2019',
  imageUrl: 'http://lorempixel.com/350/500/',
  episodes: [],
);

// episodes

final episode1 = EpisodeLink(
  identifier: "Episode 1",
  uri: Uri.parse(
    '/ds.php?file=25-saiNoJoshikousei/25-saiNoJoshikousei_Ep_01_SUB_ITA.mp4',
  ),
  downloadable: true
);
