import 'package:anime_force/src/core/component/anime/domain/website.dart';
import 'package:anime_force/src/infrastructure/parsing/html.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../framework//page_builder.dart';

void main() {
  final HtmlParser parser = HtmlParser();
  final PageBuilder pageBuilder = PageBuilder(Uri());
  final Uri website = Uri.parse('https://localhost');

  test('parse web page into anime list', () {
    var animeList = [
      AnimeLink(
        name: 'Something cool',
        url: Uri.parse('http://animeforce.com/something-cool/'),
      )
    ];

    String htmlPage = pageBuilder.buildAnimeListPage(animeList);
    expect(parser.parseAnimeListPage(htmlPage).animeList, animeList);
  });

  test('parse anime page into anime', () {
    var anime = AnimePage(
      name: 'A cool anime',
      genre: ['One', 'Second', 'Third'],
      plot: 'A simple plot of a cool anime',
      yearOfPublication: '2020',
      imageUrl: 'https://lorempixel.com/350/500/',
      episodes: [
        EpisodeLink(
          identifier: "Episode 1",
          uri: Uri.parse(
            'https://localhost/ds.php?file=25-saiNoJoshikousei/25-saiNoJoshikousei_Ep_01_SUB_ITA.mp4',
          ),
        )
      ],
    );
    String htmlPage = pageBuilder.buildAnimeDetailPage(anime);
    var parsedAnime = parser.parseAnimePage(htmlPage, website);

    expect(parsedAnime, equals(anime));
  });

  test('episodes when fetched always returns complete url of website', () {
    var anime = AnimePage(
      name: '',
      genre: [],
      plot: '',
      yearOfPublication: '',
      imageUrl: 'https://lorempixel.com/350/500/',
      episodes: [
        EpisodeLink(
          identifier: "Episode 1",
          uri: Uri.parse(
            '//localhost/ds.php?file=25-saiNoJoshikousei/25-saiNoJoshikousei_Ep_01_SUB_ITA.mp4',
          ),
        ),
        EpisodeLink(
          identifier: "Episode 2",
          uri: Uri.parse(
            '/ds.php?file=25-saiNoJoshikousei/25-saiNoJoshikousei_Ep_01_SUB_ITA.mp4',
          ),
        ),
      ],
    );

    String htmlPage = pageBuilder.buildAnimeDetailPage(anime);
    var parsedAnime = parser.parseAnimePage(htmlPage, website);

    var competeUri = Uri.parse(
        'https://localhost/ds.php?file=25-saiNoJoshikousei/25-saiNoJoshikousei_Ep_01_SUB_ITA.mp4');
    expect(parsedAnime.episodes[0].uri, equals(competeUri));
    expect(parsedAnime.episodes[1].uri, equals(competeUri));
  });

  test('strip useless Sub Ita & Streaming text from anime name', () {
    var animeList = [
      AnimeLink(
        name: 'Cool Anime Sub Ita Download & Streaming',
        url: Uri.parse('http://animeforce.com/cool-anime/'),
      )
    ];

    String htmlPage = pageBuilder.buildAnimeListPage(animeList);
    List<AnimeLink> parsed = parser.parseAnimeListPage(htmlPage).animeList;

    expect(parsed.length, 1);
    expect(parsed[0].name, 'Cool Anime');
  });

  test('Empty web page cannot be parsed', () {
    var list = parser.parseAnimeListPage('');
    expect(list.animeList, []);

    var anime = parser.parseAnimePage('', website);
    expect(anime, null);
  });
}
