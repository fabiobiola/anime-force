import 'package:anime_force/src/core/component/anime/domain/website.dart';
import 'package:test/test.dart';

import '../../../framework/page_builder.dart';

void main() {
  final pageBuilder = PageBuilder(Uri());

  test('Redirect url generated correctly', () {
    var htmlPage = pageBuilder.buildAnimeDetailPage(AnimePage(
      name: 'Name',
      imageUrl: 'https://image.server.com/cool-image.png',
      plot: 'A plot',
      genre: ['Genre1', 'Genre2'],
      yearOfPublication: '2019',
      episodes: [
        EpisodeLink(
          identifier: 'Episode 1',
          uri: Uri.parse('/ds.php?file=K/K_Ep_01_SUB_ITA.mp4'),
          downloadable: true,
        ),
      ],
    ));

    expect(htmlPage, contains('href="/ds.php?file=K/K_Ep_01_SUB_ITA.mp4"'));
  });

  test('Video page contains mp4 source', () {
    var episode =
        StreamingPage(Uri.parse('https://www.mp4.com/ANIME/K/K_Ep_01_SUB_ITA.mp4'));
    var htmlPage = pageBuilder.buildEpisodeVideoPage(episode);

    expect(
        htmlPage,
        contains(
            '<source src="${episode.source.toString()}" type="video/mp4">'));
  });
}
