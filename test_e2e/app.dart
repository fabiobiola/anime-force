import 'dart:io';

import 'package:anime_force/src/presentation/ui/core/component/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_driver/driver_extension.dart';

import 'framework/fake_website.dart';

void main() async {
  enableFlutterDriverExtension();

  var websiteUrl;
  if (Platform.isIOS) {
    websiteUrl = 'http://127.0.0.1:$defaultServerPort/';
  } else if (Platform.isAndroid) {
    /// We don't use localhost because the simulator will use itself.
    /// Check out https://developer.android.com/studio/run/emulator-networking.html for details
    websiteUrl = 'http://10.0.2.2:$defaultServerPort/';
  } else {
    throw Exception('Unsupported platform');
  }

  var myApp = MyApp(websiteUrl);
  runApp(myApp);
}
