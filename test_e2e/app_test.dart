import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';
import 'package:glob/glob.dart';

import 'framework/fake_website.dart';
import 'steps/anime_page_step.dart';
import 'steps/list_result_step.dart';
import 'steps/player_opens_step.dart';
import 'steps/search_step.dart';
import 'steps/see_page_step.dart';

/// For now only android simulator are supported
/// This is because the fake server must know the host to use for generating links,
/// but we can't know if we are using an iOS or Android simulator from the BDD tests
Future<void> main() async {
  final config = FlutterTestConfiguration()
    ..features = [Glob(r"test_e2e/features/**.feature")]
    ..reporters = [
      ProgressReporter(),
      TestRunSummaryReporter(),
      JsonReporter(path: './report.json'),
    ] // you can include the "StdoutReporter()" without the message level parameter for verbose log information
    ..hooks = [
      AttachScreenshotOnFailedStepHook(),
    ]
    ..stepDefinitions = [
      GivenISeePage(),
      GivenIamOnAnimePage(),
      ThenIExpectThePlayerOpens(),
      ThenISeeNListResult(),
      WhenISearch(),
    ]
    ..customStepParameterDefinitions = []
    ..restartAppBetweenScenarios = true
    ..targetAppPath = "test_e2e/app.dart"
    // ..tagExpression = "@smoke" // uncomment to see an example of running scenarios based on tag expressions
    // ..logFlutterProcessOutput = true
    ..exitAfterTestRun = true; // set to false if debugging to exit cleanly

  var website = FakeAnimeForceWebsite(Uri.parse('http://10.0.2.2:$defaultServerPort/'));

  await website.start();
  await GherkinRunner().execute(config);
  website.shutdown();
}
