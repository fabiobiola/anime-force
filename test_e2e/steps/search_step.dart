import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

class WhenISearch extends When1WithWorld<String, FlutterWorld> {
  @override
  RegExp get pattern => RegExp(r"I search {String}");

  @override
  Future<void> executeStep(String query) async {
    final searchField = find.byType('TextField');
    await FlutterDriverUtils.enterText(world.driver, searchField, query);
  }
}
