import 'package:gherkin/gherkin.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:flutter_driver/flutter_driver.dart';

class ThenISeeNListResult extends Then2WithWorld<int, String, FlutterWorld> {
  @override
  RegExp get pattern =>
      RegExp(r'I expect to see {int} result for the list {string}');

  @override
  Future<void> executeStep(int numberOfResults, String listName) async {
    expect(
      await FlutterDriverUtils.isPresent(
          find.byValueKey(listName), world.driver),
      true,
      reason: 'List not found, are you sure you are on the right page?',
    );

    expect(
      await FlutterDriverUtils.isPresent(
        find.byValueKey('anime-n${numberOfResults - 1}'),
        world.driver,
      ),
      true,
      reason: 'There are less anime than there should be',
    );

    expect(
      await FlutterDriverUtils.isAbsent(
        world.driver,
        find.byValueKey('anime-n$numberOfResults'),
      ),
      true,
      reason: 'There are more anime than there should be',
    );
  }
}
