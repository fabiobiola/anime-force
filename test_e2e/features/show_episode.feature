Feature: As a user, I want to see the episodes of the anime I follow

  Scenario: Open an anime
    Given I see the "Anime List" page
    When I tap the "Black Clover" text
    Then I see the "Anime Detail" page
    And I expect the "name" to be "Black Clover"

  Scenario: Search an anime
    Given I see the "Anime List" page
    When I tap the "search button" icon
    And I search "Black Clover"
    Then I expect to see 1 result for the list "Anime List"

  Scenario: Open an episode
    Given I am on "Black Clover" anime page
    When I tap the "Episode 1" text
    Then I expect the player will open
